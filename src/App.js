import "./App.css";
import Layout from "./BaiTapThucHanhLayout/Layout";

function App() {
  return (
    <div>
      <Layout />
    </div>
  );
}

export default App;
